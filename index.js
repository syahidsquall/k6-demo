import { group } from 'k6';
import { check } from "k6";
import http from "k6/http";
import { Rate } from "k6/metrics";

export let errorRate = new Rate("errors");

export default function() {
const BASE_URL = "http://mochamadsyah.id";
const PATH = "/"
const payload = JSON.stringify(['node1','node2','node3'])

  group('user flow: new user', function() {
    group('visit homepage', function() {
        let res = http.get(BASE_URL+PATH);
        check(res, {
            "counters status 200": (r) => r.status === 200
        }) || errorRate.add(1);
    
        if (res.status != 200) {
            console.log(res.body);
            console.log(details[0]);
            console.log(details[1]);
        }
    });
    group('post node', function() {
        let params = {
            "Content-Type": "application/json",
        };
    
        let res = http.post(BASE_URL+PATH+"getnode",payload, { headers: params });
        check(res, {
            "counters status 200": (r) => r.status === 200
        }) || errorRate.add(1);
    
        if (res.status != 200) {
            // console.log("status counter was " + String(res.status));
            console.log(res.body);
            console.log(details[0]);
            console.log(details[1]);
        }
    });
  });
}