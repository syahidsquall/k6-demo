## sample running command
## docker run -it --rm=true syahidsquall/testk6 run -u 10 -d 10s

FROM golang:alpine AS builder

RUN apk --update --no-cache add git
RUN go get -u github.com/loadimpact/k6
ENV PATH ${GOPATH}/bin:${PATH}

FROM alpine:latest

WORKDIR /app
ADD . /app

COPY --from=builder /go/bin/k6 /bin/