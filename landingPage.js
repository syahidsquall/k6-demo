import { check } from "k6";
import http from "k6/http";
import { Rate } from "k6/metrics";

export let errorRate = new Rate("errors");

const BASE_URL = "http://mochamadsyah.id";
const PATH = "/"

export default function () {

    let res = http.get(BASE_URL+PATH);
    check(res, {
        "counters status 200": (r) => r.status === 200
    }) || errorRate.add(1);

    if (res.status != 200) {
        console.log(res.body);
        console.log(details[0]);
        console.log(details[1]);
    }
};