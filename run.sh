#!/bin/bash

REPLICAS_DIV=10
check_cluster_health(){
    echo "checking cluster health..."
#  gcloud container clusters describe spid-production-cluster --zone=asia-southeast1-a | grep currentNodeCount | awk '{print $2}'
}

##apply resize if needed
resize_cluster(){
    echo "adjusting cluster size for run the load to $1"
    # gcloud container clusters resize spid-research-cluster --node-pool preemptible-pool --num-nodes $argument
    sleep 10
}

deploy_services() {
    VUSparam=$1
    if [[ $VUSparam -lt 10 ]]; then 
        echo "minimum is 10 vus, set it to 10";
        VUSparam=10
    fi
    echo "updating k8s template with adjusted VUS value..."
    cp manifest/kube/job.yaml.template manifest/kube/job.yaml
    VUS=$(( VUSparam / REPLICAS_DIV ))
    echo "adjusting replicas to $VUS"
    sed -i "s/parallelism: 1/parallelism: $VUS/g" manifest/kube/job.yaml
    echo "pushing job to kubernetes cluster"
    export KUBECONFIG=/app/config-local
    kubectl apply -f manifest/kube/job.yaml
}

##load test argument first = user-vus, second = duration
load_test(){
    echo "running loadtest on LandingPage..."
    k6 -d $1s --vus 10 run --out influxdb=http://localhost:8086/db0 landingPage.js
    # sleep 10
    # echo "running loadtest on nodePage..."
    # k6 -d $1s --vus 10 run --out influxdb=http://localhost:8086/db0 nodePage.js
    # sleep 10
    echo "finishing loadtest..."
    # cleanup
}

cleanup(){
    echo "Cleaning up..."
    kubectl delete -f manifest/kube/job.yaml
    # kubectl delete -f manifest/kube/deployment.yaml
    # kubectl delete -f manifest/kube/services.yaml
}

# check_cluster_health
# resize_cluster 5
# deploy 100
# load_test 100 60
# resize_cluster 3
# cleanup