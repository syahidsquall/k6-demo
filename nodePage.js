import { check } from "k6";
import http from "k6/http";
import { Rate } from "k6/metrics";

export let errorRate = new Rate("errors");

const BASE_URL = "http://demo.mochamadsyah.id";
const PATH = "/getnode"
const payload = JSON.stringify(['node1','node2','node3'])

export default function () {

    let params = {
        "Content-Typ    e": "application/json",
    };

    let res = http.post(BASE_URL+PATH,payload, { headers: params });
    check(res, {
        "counters status 200": (r) => r.status === 200
    }) || errorRate.add(1);

    if (res.status != 200) {
        // console.log("status counter was " + String(res.status));
        console.log(res.body);
        console.log(details[0]);
        console.log(details[1]);
    }
};